export default {
    PANEL_TOGGLE: 'panelToggle',

    PANEL_CHANGE_DISPLAY: 'panelChangeDisplay',

    FLIGHT_PLAN_ADD_TO_TABLE: 'flightPlanAddToTable',

    FLIGHT_PLAN_REMOVE: 'flightPlanRemove',

    FLIGHT_PLAN_RENDER_ALL: 'flightPlanRenderAll',

    FLIGHT_PLAN_PREDICT_ALL: 'flightPlanPredictAll',

    FLIGHT_PLAN_CLEAR_ALL: 'flightPlanClearAll',

    AERODROME_LOAD_DEPARTURE_AERODROMES: 'aerodromeLoadDepartureAerodromes',

    AERODROME_SELECT_DEPARTURE_AERODROME: 'aerodromeSelectDepartureAerodrome',

    AERODROME_HOVER_DEPARTURE_AERODROME: 'aerodromeHoverDepartureAerodrome',

    AERODROME_CLEAR_LOADED_DEPARTURE_AERODROMES: 'aerodromeClearLoadedDepartureAerodromes',

    AERODROME_CLEAR_SELECTED_DEPARTURE_AERODROME: 'aerodromeClearSelectedDepartureAerodrome',

    AERODROME_LOAD_ARRIVAL_AERODROMES: 'aerodromeLoadArrivalAerodromes',

    AERODROME_SELECT_ARRIVAL_AERODROME: 'aerodromeSelectArrivalAerodrome',

    AERODROME_HOVER_ARRIVAL_AERODROME: 'aerodromeHoverArrivalAerodrome',

    AERODROME_CLEAR_LOADED_ARRIVAL_AERODROMES: 'aerodromeClearLoadedArrivalAerodromes',

    AERODROME_CLEAR_SELECTED_ARRIVAL_AERODROME: 'aerodromeClearSelectedArrivalAerodrome',

    AERODROME_SEARCH_MODAL_UPDATE_CURRENT_SELECTED_AERODROME_ID: 'aerodromeSearchModalUpdateCurrentSelectedAerodromeId',

    CUSTOM_FLIGHT_PLAN_CREATE: 'customFlightPlanCreate',

    CUSTOM_FLIGHT_PLAN_CLEAR: 'customFlightPlanClear',

    WAYPOINTS_LOAD: 'waypointsLoad',

    WAYPOINTS_CLEAR_LOADED: 'waypointsClearLoaded',

    WAYPOINTS_ADD_TO_LIST: 'waypointsAddToList',

    WAYPOINTS_REMOVE_FROM_LIST: 'waypointsRemoveFromList',

    WAYPOINTS_CLEAR_LIST: 'waypointsClearList',

    PROGRESS_INDICATOR_TOGGLE: 'progressIndicatorToggle',

    ALERT_TOGGLE: 'alertToggle',

    INFORMATION_TOGGLE: 'informationToggle',

    MAP_UPDATE_CLICK_COORDINATE: 'mapUpdateClickCoordinate',

};