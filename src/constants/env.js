export default {
    serverUrl: window.flightPlanResearchEnvironment.serverUrl,
    displayGeneralInformationOnLoad: window.flightPlanResearchEnvironment.displayGeneralInformationOnLoad,
}