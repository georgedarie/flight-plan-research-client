import 'bootstrap/dist/css/bootstrap.min.css';
import Vue from 'vue';
import VModal from 'vue-js-modal';
import VueLayers from 'vuelayers';
import {Feature} from 'vuelayers';
import {PointGeom} from 'vuelayers';
import {LineStringGeom} from 'vuelayers';
import {StyleBox} from 'vuelayers';
import {StrokeStyle} from 'vuelayers';
import {IconStyle } from 'vuelayers';
import store from './store/';
import App from './App.vue';

Vue.config.productionTip = false;
Vue.use(VueLayers);
Vue.use(Feature);
Vue.use(PointGeom);
Vue.use(LineStringGeom);
Vue.use(StyleBox);
Vue.use(StrokeStyle);
Vue.use(IconStyle);
Vue.use(VModal, { dialog: true, dynamic: true, injectModalsContainer: true });

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
