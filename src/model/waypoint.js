const fields = [
    {
        displayedName: '',
        name: 'id',
        display: false,
    },
    {
        displayedName: 'Designator',
        name: 'designator',
        display: true,
    },
    {
        displayedName: '',
        name: 'name',
        display: false,
    },
    {
        displayedName: '',
        name: 'position',
        display: false,
    },
];

const numberOfWaypointsOptions = [
    {
        numericValue: '4',
        apiCallValue: 'FOUR'
    },
    {
        numericValue: '9',
        apiCallValue: 'NINE'
    },
    {
        numericValue: '16',
        apiCallValue: 'SIXTEEN'
    },
    {
        numericValue: '25',
        apiCallValue: 'TWENTY_FIVE'
    },
    {
        numericValue: '36',
        apiCallValue: 'THIRTY_SIX'
    },
    {
        numericValue: '49',
        apiCallValue: 'FORTY_NINE'
    },
    {
        numericValue: '64',
        apiCallValue: 'SIXTY_FOUR'
    },
    {
        numericValue: '81',
        apiCallValue: 'EIGHTY_ONE'
    },
    {
        numericValue: '100',
        apiCallValue: 'ONE_HUNDRED'
    },
];

export default {
    name: 'Waypoint',
    fields,
    numberOfWaypointsOptions
}