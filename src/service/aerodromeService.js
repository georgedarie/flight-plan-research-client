import env from '../constants/env';
import ServiceConfig from "./serviceConfig";

const searchAerodromeEndpoint = 'getSimpleAerodromesByTextQuery';
const searchAerodromesWhichArePartOfFlightPlanEndpoint = 'getAerodromesWhichArePartOfFlightPlan';
const serverUrl = 'http://' + env.serverUrl;

function getAerodromes(config) {
    const url = serverUrl + '/' + searchAerodromeEndpoint + '?textQuery=' + config.textQuery;
    getAerodromesCall(config, url);
}

function getAerodromesWhichArePartOfFlightPlans(config) {
    let url = serverUrl + '/' + searchAerodromesWhichArePartOfFlightPlanEndpoint + '?query=' + config.textQuery;
    url += config.isDeparture ? '&isDeparture=true' : '&isDeparture=false';
    url += config.otherDesignator === null ? '' : `&otherAerodromeDesignator=${config.otherDesignator}`;
    getAerodromesCall(config, url);
}

function getAerodromesCall(config, url) {
    fetch(url, {
        method: 'GET',
        signal: ServiceConfig.getAbortSignal(),
    }).then(resp => {
        if (resp.status === 200) {
            config.onSuccess(resp);
        } else {
            config.onError(resp);
        }
    })
        .catch(error => config.onError(error));
}

export default {
    getAerodromes,
    getAerodromesWhichArePartOfFlightPlans
}