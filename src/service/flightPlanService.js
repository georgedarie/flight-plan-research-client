import env from '../constants/env';
import ServiceConfig from "./serviceConfig";

const searchFlightPlanEndpoint = 'getSimpleFlightPlanByDepartureAndArrival';
const predictFlightPlansEndpoint = 'getPredictedFlightPlans';
const serverUrl = 'http://' + env.serverUrl;

function getFlightPlan(config) {
    const url = `${serverUrl}/${searchFlightPlanEndpoint}?` +
        `departureAerodromeDesignator=${config.departureAerodromeDesignator}&` +
        `arrivalAerodromeDesignator=${config.arrivalAerodromeDesignator}`;
    fetch(url, {
       method: 'GET',
        signal: ServiceConfig.getAbortSignal(),
    }).then(resp => {
        if (resp.status === 200) {
            config.onSuccess(resp);
        } else {
            config.onError(resp);
        }
    })
        .catch(error => config.onError(error));
}

function getPredictedFlightPlans(config) {
    let {flightPlansToPredict} = config;
    let body = JSON.stringify(flightPlansToPredict);
    const url = `${serverUrl}/${predictFlightPlansEndpoint}`;
    fetch(url, {
        method: 'POST',
        body: body,
        signal: ServiceConfig.getAbortSignal(),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(resp => {
        if (resp.status === 200) {
            config.onSuccess(resp);
        } else {
            config.onError(resp);
        }
    })
        .catch(error => config.onError(error));
}

export default {
    getFlightPlan,
    getPredictedFlightPlans
}