import Store from '../store';

function getAbortSignal() {
    return Store.state.progressIndicatorStore.signal;
}

export default {
    getAbortSignal
}