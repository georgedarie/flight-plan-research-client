import env from '../constants/env';
import ServiceConfig from './serviceConfig';

const searchSimpleWaypointsEndpoint = 'getSimpleWaypointsByDesignators';
const searchWaypointsBetweenAirports = 'getWaypointsBetweenAerodromes';
const serverUrl = 'http://' + env.serverUrl;

function getWaypoints(config) {
    const url = `${serverUrl}/${searchSimpleWaypointsEndpoint}?` +
        `departureAerodromeDesignator=${config.departureAerodromeDesignator}`;
    const body = JSON.stringify(config.waypointDesignators);
    fetch(url, {
        method: 'POST',
        body: body,
        signal: ServiceConfig.getAbortSignal(),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(resp => {
        if (resp.status === 200) {
            config.onSuccess(resp);
        } else {
            config.onError(resp);
        }
    })
        .catch(error => config.onError(error));
}

function getWaypointsBetweenAirports(config) {
    const url = `${serverUrl}/${searchWaypointsBetweenAirports}?` +
        `departureAerodromeDesignator=${config.departureAerodromeDesignator}&` +
        `arrivalAerodromeDesignator=${config.arrivalAerodromeDesignator}&` +
        `numberOfWaypoints=${config.numberOfWaypoints}`;
    fetch(url, {
        method: 'GET',
        signal: ServiceConfig.getAbortSignal(),
    }).then(resp => {
        if (resp.status === 200) {
            config.onSuccess(resp);
        } else {
            config.onError(resp);
        }
    })
        .catch(error => config.onError(error));
}

export default {
    getWaypoints,
    getWaypointsBetweenAirports
}