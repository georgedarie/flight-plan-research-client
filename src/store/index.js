import Vue from 'vue';
import Vuex from 'vuex';
import PanelStore from './modules/panel'
import FlightPlansStore from './modules/flightPlans';
import AerodromeStore from './modules/aerodromes';
import CustomFlightPlanStore from './modules/customFlightPlan';
import WaypointsStore from './modules/waypoints';
import ProgressIndicatorStore from "./modules/progressIndicator";
import AlertStore from './modules/alert';
import InformationStore from "./modules/information";
import MapStore from './modules/map';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        panelStore: PanelStore,
        flightPlansStore: FlightPlansStore,
        aerodromeStore: AerodromeStore,
        customFlightPlanStore: CustomFlightPlanStore,
        waypointsStore: WaypointsStore,
        progressIndicatorStore: ProgressIndicatorStore,
        alertStore: AlertStore,
        informationStore: InformationStore,
        mapStore: MapStore,
    }
})