import A from '../../constants/actions';
import M from '../../constants/mutations';
import AerodromeService from '../../service/aerodromeService';

const state = {
    loadedDepartureAerodromes: [],
    selectedDepartureAerodrome: null,
    hoveredDepartureAerodrome: null,
    loadedArrivalAerodromes: [],
    selectedArrivalAerodrome: null,
    hoveredArrivalAerodrome: null,
};

const actions = {
    [A.AERODROME_LOAD_DEPARTURE_AERODROMES]({commit}, inputConfig) {
        let config = {
            textQuery: inputConfig.textQuery,
            isDeparture: inputConfig.isDeparture,
            otherDesignator: inputConfig.otherDesignator,
            onSuccess(resp) {
                resp.json()
                    .then(data => {
                        commit(M.AERODROME_LOAD_DEPARTURE_AERODROMES, data);
                    })
                    .catch(err => {
                        console.error(err);
                    });
            },
            onError(err) {
                console.error(err);
                commit(M.AERODROME_CLEAR_LOADED_DEPARTURE_AERODROMES);
            }
        };
        if (inputConfig.onlyAirportsInFlightPlans) {
            AerodromeService.getAerodromesWhichArePartOfFlightPlans(config);
        } else {
            AerodromeService.getAerodromes(config);
        }
    },
    [A.AERODROME_SELECT_DEPARTURE_AERODROME]({commit}, aerodromeId) {
        commit(M.AERODROME_SELECT_DEPARTURE_AERODROME, aerodromeId);
    },
    [A.AERODROME_HOVER_DEPARTURE_AERODROME]({commit}, index) {
        commit(M.AERODROME_HOVER_DEPARTURE_AERODROME, index);
    },
    [A.AERODROME_CLEAR_LOADED_DEPARTURE_AERODROMES]({commit}) {
        commit(M.AERODROME_CLEAR_LOADED_DEPARTURE_AERODROMES);
    },
    [A.AERODROME_CLEAR_SELECTED_DEPARTURE_AERODROME]({commit}) {
        commit(M.AERODROME_CLEAR_SELECTED_DEPARTURE_AERODROME);
    },
    [A.AERODROME_LOAD_ARRIVAL_AERODROMES]({commit}, inputConfig) {
        let config = {
            textQuery: inputConfig.textQuery,
            isDeparture: inputConfig.isDeparture,
            otherDesignator: inputConfig.otherDesignator,
            onSuccess(resp) {
                resp.json()
                    .then(data => {
                        commit(M.AERODROME_LOAD_ARRIVAL_AERODROMES, data);
                    })
                    .catch(err => {
                        console.error(err);
                    });
            },
            onError(err) {
                console.error(err);
                commit(M.AERODROME_LOAD_ARRIVAL_AERODROMES);
            }
        };
        if (inputConfig.onlyAirportsInFlightPlans) {
            AerodromeService.getAerodromesWhichArePartOfFlightPlans(config);
        } else {
            AerodromeService.getAerodromes(config);
        }
    },
    [A.AERODROME_SELECT_ARRIVAL_AERODROME]({commit}, aerodromeId) {
        commit(M.AERODROME_SELECT_ARRIVAL_AERODROME, aerodromeId);
    },
    [A.AERODROME_HOVER_ARRIVAL_AERODROME]({commit}, index) {
        commit(M.AERODROME_HOVER_ARRIVAL_AERODROME, index);
    },
    [A.AERODROME_CLEAR_LOADED_ARRIVAL_AERODROMES]({commit}) {
        commit(M.AERODROME_CLEAR_LOADED_ARRIVAL_AERODROMES);
    },
    [A.AERODROME_CLEAR_SELECTED_ARRIVAL_AERODROME]({commit}) {
        commit(M.AERODROME_CLEAR_SELECTED_ARRIVAL_AERODROME);
    },
};

const mutations = {
    [M.AERODROME_LOAD_DEPARTURE_AERODROMES](state, loadedAerodromes) {
        state.loadedDepartureAerodromes = loadedAerodromes;
    },
    [M.AERODROME_SELECT_DEPARTURE_AERODROME](state, aerodromeId) {
        state.selectedDepartureAerodrome =
            state.loadedDepartureAerodromes.find(aerodrome => aerodrome.id === aerodromeId);
    },
    [M.AERODROME_HOVER_DEPARTURE_AERODROME](state, index) {
        state.hoveredDepartureAerodrome = state.loadedDepartureAerodromes[index];
    },
    [M.AERODROME_CLEAR_LOADED_DEPARTURE_AERODROMES](state) {
        state.loadedDepartureAerodromes = [];
    },
    [M.AERODROME_CLEAR_SELECTED_DEPARTURE_AERODROME](state) {
        state.selectedDepartureAerodrome = null;
        state.hoveredDepartureAerodrome = null;
    },
    [M.AERODROME_LOAD_ARRIVAL_AERODROMES](state, loadedAerodromes) {
        state.loadedArrivalAerodromes = loadedAerodromes;
    },
    [M.AERODROME_SELECT_ARRIVAL_AERODROME](state, aerodromeId) {
        state.selectedArrivalAerodrome =
            state.loadedArrivalAerodromes.find(aerodrome => aerodrome.id === aerodromeId);
    },
    [M.AERODROME_HOVER_ARRIVAL_AERODROME](state, index) {
        state.hoveredArrivalAerodrome = state.loadedArrivalAerodromes[index];
    },
    [M.AERODROME_CLEAR_LOADED_ARRIVAL_AERODROMES](state) {
        state.loadedArrivalAerodromes = [];
    },
    [M.AERODROME_CLEAR_SELECTED_ARRIVAL_AERODROME](state) {
        state.selectedArrivalAerodrome = null;
        state.hoveredArrivalAerodrome = null;
    },
};

export default {
    state,
    actions,
    mutations
}