import A from '../../constants/actions';
import M from '../../constants/mutations';

const state = {
    display: false,
    title: '',
    text: '',
};

const actions = {
  [A.ALERT_TOGGLE]({commit}, config) {
      commit(M.ALERT_TOGGLE, config);
  },
};

const mutations = {
    [M.ALERT_TOGGLE](state, config) {
        const {display} = config;
        const {title} = config;
        const {text} = config;
        state.display = display;
        state.title = title;
        state.text = text;
    }
};

export default {
    state,
    actions,
    mutations
}