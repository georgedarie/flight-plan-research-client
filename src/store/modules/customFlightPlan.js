import A from '../../constants/actions';
import M from '../../constants/mutations';
import FlightPlanService from '../../service/flightPlanService';
import Store from "../index";

const state = {
    createdCustomFlightPlan: null,
    predictionOfCustomFlightPlan: null,
};

const uuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

const getRoute = (departureAirport, arrivalAirport, waypoints) => {
    let coordinates = [];
    coordinates.push([
        departureAirport.position.longitude,
        departureAirport.position.latitude
    ]);
    waypoints.forEach(waypoint => {
        coordinates.push([
            waypoint.position.longitude,
            waypoint.position.latitude
        ]);
    });
    coordinates.push([
        arrivalAirport.position.longitude,
        arrivalAirport.position.latitude
    ]);
    return {
        type: 'LineString',
        coordinates
    };

};

const getRouteElements = (departureAirport, arrivalAirport, waypoints) => {
    let routeElements = [];
    departureAirport.isAirport = true;
    departureAirport.isDeparture = true;
    arrivalAirport.isAirport = true;
    routeElements.push(departureAirport);
    routeElements = routeElements.concat(waypoints.map(wp => {
        wp.canAdd = false;
        return wp;
    }));
    routeElements.push(arrivalAirport);

    return routeElements;
};

const actions = {
    [A.CUSTOM_FLIGHT_PLAN_CREATE]({commit}, payload) {
        let {departureAirport} = payload;
        let {arrivalAirport} = payload;
        let {waypoints} = payload;
        let route = getRoute(departureAirport, arrivalAirport, waypoints);
        let routeElements = getRouteElements(departureAirport, arrivalAirport, waypoints);
        let flightPlanToPredict = {
            id: uuid(),
            arrivalAirport: arrivalAirport.designator,
            departureAirport: departureAirport.designator,
            route
        };
        let newCustomFlightPlan = {
            departureAirport: departureAirport.designator,
            arrivalAirport: arrivalAirport.designator,
            waypoints,
            route,
            routeElements,
            isFlightPlan: true,
            isPrediction: false,
            isCustom: true
        };
        let config = {
            flightPlansToPredict: [flightPlanToPredict],
            onSuccess(resp) {
                resp.json()
                    .then(predictions => {
                        let predictedFlightPlan = predictions[0];
                        predictedFlightPlan.isFlightPlan = true;
                        predictedFlightPlan.isPrediction = true;
                        predictedFlightPlan.routeElements = predictedFlightPlan.waypoints;
                        commit(M.CUSTOM_FLIGHT_PLAN_CREATE, {
                            newCustomFlightPlan,
                            predictedFlightPlan
                        });
                        Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                    })
                    .catch(err => {
                        Store.dispatch(A.ALERT_TOGGLE, {
                            display: true,
                            title: 'Warning!',
                            text: 'An error occurred while predicting the custom flight plan.'
                        });
                        Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                        console.error(err);
                    })
            },
            onError(err) {
                if (err.status) {
                    Store.dispatch(A.ALERT_TOGGLE, {
                        display: true,
                        title: 'Warning!',
                        text: 'An error occurred while predicting the custom flight plan.'
                    });
                    Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                    console.error(err);
                }
            }
        };
        FlightPlanService.getPredictedFlightPlans(config);
    },
    [A.CUSTOM_FLIGHT_PLAN_CLEAR]({commit}) {
        commit(M.CUSTOM_FLIGHT_PLAN_CLEAR);
    },
};

const mutations = {
    [M.CUSTOM_FLIGHT_PLAN_CREATE](state, config) {
        let {newCustomFlightPlan} = config;
        let {predictedFlightPlan} = config;
        state.createdCustomFlightPlan = newCustomFlightPlan;
        state.predictionOfCustomFlightPlan = predictedFlightPlan;
    },
    [M.CUSTOM_FLIGHT_PLAN_CLEAR](state) {
        state.createdCustomFlightPlan = null;
        state.predictionOfCustomFlightPlan = null;
    },
};

export default {
    state,
    actions,
    mutations
}