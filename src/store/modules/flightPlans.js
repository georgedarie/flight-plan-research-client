import A from '../../constants/actions';
import M from '../../constants/mutations';
import FlightPlanService from '../../service/flightPlanService';
import WaypointService from '../../service/waypointService';
import Store from '../index';

const state = {
    elementsInTable: [],
    elementsOnMap: [],
};

const fetchWaypoints = (flightPlanData, departureAerodrome, arrivalAerodrome, commit) => {
    if (flightPlanData === null) {
        Store.dispatch(A.ALERT_TOGGLE, {
            display: true,
            title: 'Warning!',
            text: 'No flight plan found for selected departure and arrival airports.'
        });
        return;
    }
    let departureAerodromeDesignator = departureAerodrome.designator;
    let config = {
        departureAerodromeDesignator,
        waypointDesignators: flightPlanData.waypoints,
        onSuccess(resp) {
            resp.json()
                .then(waypointsData => {
                    let flightPlan = flightPlanData;
                    flightPlan.routeElements = [];
                    departureAerodrome.isAirport = true;
                    departureAerodrome.isDeparture = true;
                    arrivalAerodrome.isAirport = true;
                    flightPlan.routeElements.push(departureAerodrome);
                    flightPlan.routeElements = flightPlan.routeElements.concat(waypointsData);
                    flightPlan.routeElements.push(arrivalAerodrome);
                    flightPlan.departureAirport = departureAerodrome;
                    flightPlan.arrivalAirport = arrivalAerodrome;
                    commit(M.FLIGHT_PLAN_ADD_TO_TABLE, flightPlan);
                    Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                })
                .catch(err => {
                    Store.dispatch(A.ALERT_TOGGLE, {
                        display: true,
                        title: 'Warning!',
                        text: 'An error occurred while fetching the flight plan.'
                    });
                    Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                    console.error(err);
                })
        },
        onError(err) {
            if (err.status) {
                Store.dispatch(A.ALERT_TOGGLE, {
                    display: true,
                    title: 'Warning!',
                    text: 'An error occurred while fetching the flight plan.'
                });
                Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                console.error(err);
            }
        }
    };
    WaypointService.getWaypoints(config);
};

const fetchFlightPlan = (departureAerodrome, arrivalAerodrome, commit) => {
    let departureAerodromeDesignator = departureAerodrome.designator;
    let arrivalAerodromeDesignator = arrivalAerodrome.designator;
    let config = {
        departureAerodromeDesignator,
        arrivalAerodromeDesignator,
        onSuccess(res) {
            res.json()
                .then(flightPlanData => {
                    fetchWaypoints(flightPlanData, departureAerodrome, arrivalAerodrome, commit);
                })
                .catch(err => {
                    Store.dispatch(A.ALERT_TOGGLE, {
                        display: true,
                        title: 'Warning!',
                        text: 'An error occurred while fetching the flight plan.'
                    });
                    Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                    console.error(err);
                })
        },
        onError(err) {
            if (err.status) {
                Store.dispatch(A.ALERT_TOGGLE, {
                    display: true,
                    title: 'Warning!',
                    text: 'An error occurred while fetching the flight plan.'
                });
                Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                console.error(err);
            }
        }
    };
    FlightPlanService.getFlightPlan(config);
};

const actions = {
    [A.FLIGHT_PLAN_ADD_TO_TABLE]({commit}, config) {
        let {departureAirport} = config;
        let {arrivalAirport} = config;
        fetchFlightPlan(departureAirport, arrivalAirport, commit);
    },
    [A.FLIGHT_PLAN_REMOVE]({commit}, elementId) {
        commit(M.FLIGHT_PLAN_REMOVE, elementId);
    },
    [A.FLIGHT_PLAN_RENDER_ALL]({commit}) {
        commit(M.FLIGHT_PLAN_RENDER_ALL);
    },
    [A.FLIGHT_PLAN_PREDICT_ALL]({commit}, predictedFlightPlans) {
        commit(M.FLIGHT_PLAN_PREDICT_ALL, predictedFlightPlans);
    },
    [A.FLIGHT_PLAN_CLEAR_ALL]({commit}) {
        commit(M.FLIGHT_PLAN_CLEAR_ALL);
    },
};

const mutations = {
    [M.FLIGHT_PLAN_ADD_TO_TABLE](state, element) {
        if (state.elementsInTable.filter(el => el.id === element.id).length === 0) {
            state.elementsInTable.push(element);
        }
    },
    [M.FLIGHT_PLAN_REMOVE](state, elementId) {
        state.elementsInTable = state.elementsInTable.filter(element => element.id !== elementId);
        state.elementsOnMap =
            state.elementsOnMap.filter(element => element.id !== elementId && element.predictedId !== elementId);
    },
    [M.FLIGHT_PLAN_RENDER_ALL](state) {
        state.elementsInTable.forEach(element => {
            if (state.elementsOnMap.filter(el => el.id === element.id).length === 0) {
                state.elementsOnMap.push(element);
            }
        });
    },
    [M.FLIGHT_PLAN_PREDICT_ALL](state, predictedFlightPlans) {
        state.elementsInTable.forEach(element => {
            if (state.elementsOnMap.filter(el => el.id === element.id).length === 0) {
                state.elementsOnMap.push(element);
            }
        });
        predictedFlightPlans.forEach(element => {
            if (state.elementsOnMap.filter(el => el.id === element.id).length === 0) {
                state.elementsOnMap.push(element);
            }
        })
    },
    [M.FLIGHT_PLAN_CLEAR_ALL](state) {
        state.elementsInTable = [];
        state.elementsOnMap = [];
    },
};

export default {
    state,
    actions,
    mutations
}