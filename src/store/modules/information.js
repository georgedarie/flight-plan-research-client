import A from '../../constants/actions';
import M from '../../constants/mutations';
import env from "../../constants/env";

function getInfoIconSrc() {
    const images = require.context('../../assets', false, /\.svg$/);
    return images(`./info.svg`);
}

const DEFAULT_TITLE = 'Welcome!';
const DEFAULT_CONTENT = 'You can use this application to predict the actual routes of filed flight plans.<br/>' +
    'A filed flight plan is a document which a pilot has to write and it needs to contain a list of waypoints on ' +
    'which the aircraft will pass. Sometimes the aircraft will have to take another route due to various reasons. <br/>' +
    'This application offers two ways of predicting a plan: using the default flight plans which are in the database ' +
    'or creating a custom flight plan by selecting some waypoints on the map. These flight plans will be fed to an ' +
    'artificial neural network and a possible route will be rendered on the map.<br/>' +
    'On the map you can click on different features to see more information about them or, when creating a custom ' +
    'flight plan, you can click on a waypoint to add it to the list which will make up the route of that plan.<br/>' +
    'You can start by searching for a departure and an arrival airport. You can do that by starting to type in the ' +
    'search boxes located inside the panel. When you see the desired airport you need to click on it in order to select it.<br/>' +
    'Information about each action can be viewed by clicking on the <img src="' + getInfoIconSrc() + '" alt="info" width="16" height="16"/>' +
    ' icon which is present next to the buttons that trigger a specific action.';
const state = {
    display: env.displayGeneralInformationOnLoad,
    title: DEFAULT_TITLE,
    content: DEFAULT_CONTENT,
};

const actions = {
    [A.INFORMATION_TOGGLE]({commit}, config) {
        commit(M.INFORMATION_TOGGLE, config);
    }
};

const mutations = {
    [M.INFORMATION_TOGGLE](state, config) {
        const {display} = config;
        const {title} = config;
        const {content} = config;
        state.display = display;
        state.title = (title) ? title : '';
        state.content = (content) ? content: DEFAULT_CONTENT;
    }
};

export default {
    state,
    actions,
    mutations,
}