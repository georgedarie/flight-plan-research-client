import A from '../../constants/actions';
import M from '../../constants/mutations';

const state = {
    clickCoordinate: null
};

const actions = {
    [A.MAP_UPDATE_CLICK_COORDINATE]({commit}, newCoordinate) {
        commit(M.MAP_UPDATE_CLICK_COORDINATE, newCoordinate);
    }
};

const mutations = {
    [M.MAP_UPDATE_CLICK_COORDINATE](state, newCoordinate) {
        state.clickCoordinate = newCoordinate;
    }
};

export default {
    state,
    actions,
    mutations,
}