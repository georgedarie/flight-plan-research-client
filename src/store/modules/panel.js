import A from '../../constants/actions';
import M from '../../constants/mutations';

const state = {
    isOpen: false,
    displayDefaultPlansPanel: true,
};

const actions = {
    [A.PANEL_TOGGLE]({commit}) {
        commit(M.PANEL_TOGGLE);
    },
    [A.PANEL_CHANGE_DISPLAY]({commit}) {
        commit(M.PANEL_CHANGE_DISPLAY);
    },
};

const mutations = {
    [M.PANEL_TOGGLE](state) {
        state.isOpen = !state.isOpen;
    },
    [M.PANEL_CHANGE_DISPLAY](state) {
        state.displayDefaultPlansPanel = !state.displayDefaultPlansPanel;
    },
};

export default {
    state,
    actions,
    mutations
}