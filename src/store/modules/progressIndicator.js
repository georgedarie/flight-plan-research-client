import A from '../../constants/actions';
import M from '../../constants/mutations';

const DEFAULT_DISPLAYED_MESSAGE = 'Fetching data...';

const state = {
    displayIndicator: false,
    displayedMessage: DEFAULT_DISPLAYED_MESSAGE,
    canBeCancelled: true,
    abortController: null,
    signal: null,
};

const actions = {
    [A.PROGRESS_INDICATOR_TOGGLE]({commit}, config) {
        commit(M.PROGRESS_INDICATOR_TOGGLE, config);
    }
};

const mutations = {
    [M.PROGRESS_INDICATOR_TOGGLE](state, config) {
        const {toggle} = config;
        const {message} = config;
        const {canBeCancelled} = config;
        state.displayIndicator = (toggle) ? toggle : false;
        state.displayedMessage = (message) ? message : DEFAULT_DISPLAYED_MESSAGE;
        state.canBeCancelled = (canBeCancelled) ? canBeCancelled : true;
        if (canBeCancelled) {
            state.abortController = new AbortController();
            state.signal = state.abortController.signal;
        }
    }
};

export default {
    state,
    actions,
    mutations,
}