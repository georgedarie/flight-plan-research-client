import A from '../../constants/actions';
import M from '../../constants/mutations';
import WaypointService from '../../service/waypointService';
import Store from "../index";

const state = {
    loadedWaypoints: [],
    waypointsInList: []
};

const actions = {
    [A.WAYPOINTS_LOAD]({commit}, payload) {
        let {departureAerodromeDesignator} = payload;
        let {arrivalAerodromeDesignator} = payload;
        let {numberOfWaypoints} = payload;
        let config = {
            departureAerodromeDesignator,
            arrivalAerodromeDesignator,
            numberOfWaypoints,
            onSuccess(resp) {
                resp.json()
                    .then(waypoints => {
                        commit(M.WAYPOINTS_LOAD, waypoints);
                        Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                    })
                    .catch(err => {
                        Store.dispatch(A.ALERT_TOGGLE, {
                            display: true,
                            title: 'Warning!',
                            text: 'An error occurred while fetching waypoints.'
                        });
                        Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                        console.error(err);
                    });
            },
            onError(err) {
                if (err.status) {
                    Store.dispatch(A.ALERT_TOGGLE, {
                        display: true,
                        title: 'Warning!',
                        text: 'An error occurred while fetching waypoints.'
                    });
                    Store.dispatch(A.PROGRESS_INDICATOR_TOGGLE, {toggle: false});
                    console.error(err);
                }
            }
        };
        WaypointService.getWaypointsBetweenAirports(config);
    },
    [A.WAYPOINTS_CLEAR_LOADED]({commit}) {
        commit(M.WAYPOINTS_CLEAR_LOADED);
    },
    [A.WAYPOINTS_ADD_TO_LIST]({commit}, waypoint) {
        commit(M.WAYPOINTS_ADD_TO_LIST, waypoint);
    },
    [A.WAYPOINTS_REMOVE_FROM_LIST]({commit}, waypointId) {
        commit(M.WAYPOINTS_REMOVE_FROM_LIST, waypointId);
    },
    [A.WAYPOINTS_CLEAR_LIST]({commit}) {
        commit(M.WAYPOINTS_CLEAR_LIST);
    },
};

const mutations = {
    [M.WAYPOINTS_LOAD](state, waypoints) {
        state.loadedWaypoints = waypoints.map(wp => {
            wp.canAdd = true;
            return wp;
        });
    },
    [M.WAYPOINTS_CLEAR_LOADED](state) {
        state.loadedWaypoints = [];
    },
    [M.WAYPOINTS_ADD_TO_LIST](state, waypoint) {
        if (state.waypointsInList.filter(wp => wp.id === waypoint.id).length === 0) {
            state.waypointsInList.push(waypoint);
        }
    },
    [M.WAYPOINTS_REMOVE_FROM_LIST](state, waypointId) {
        state.waypointsInList = state.waypointsInList.filter(waypoint => waypoint.id !== waypointId);
    },
    [M.WAYPOINTS_CLEAR_LIST](state) {
        state.waypointsInList = [];
    },
};

export default {
    state,
    actions,
    mutations
}