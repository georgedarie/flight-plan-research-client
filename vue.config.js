module.exports = {
  lintOnSave: false,
  chainWebpack: config => {
    config.module
        .rule('vue')
        .use('vue-loader')
        .loader('vue-loader')
        .tap(options => {
          return {
            'vl-style-icon': 'src',
            ...options
          }
        })
  }
}
